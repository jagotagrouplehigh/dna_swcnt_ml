function save_seq2fv(readFile,n,fvtype,saveFile)
% This function reads a csv file of the sequence training sets then convert
% to the position specific vector (psv) or term frequency vector (tfv) with
% overlapping n-gram analysis. Then, save the file as arff format for WEKA.
% Input variables:
% readFile: input filename 
% n: number of characters in a single unit of psv or tfv
% fvtype: 'psv' for position specific vector, and 'tfv' for term frequency
% vector
% 
% 'allcomb' function is required to run this function.
% 'allcomb' function can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/10064-allcomb-varargin
%
% Example: trigram term frequency vector
% n = 3;
% fvtype = 'tfv';
% readFile = './seq_TC_12mer.csv';
% path_tr = './trainingSet/';
% saveFile = [path_tr 'TC_' fvtype '_' num2str(n) 'gram.csv'];
% save_seq2fv(readFile,n,fvtype,saveFile)

vocab = genVocab(n);

if fvtype == 'psv'
    if size(readFile, 1) == 1
        % Import the sequence set from csv file
        rawText = textread(readFile, '%s', 'whitespace',',');
        % Sequence
        seq = rawText(1:2:length(rawText));
        % Class
        class = rawText(2:2:length(rawText));
    else
        % Sequence
        seq = readFile(:,1);
        % Class
        class = readFile(:,2);
    end

    % Sequence length
    l_seq = length(seq{1});

    for i=1:l_seq(1)
        seq_TC_char(:,i) = cellfun(@(x) x(i),seq(cellfun('length',seq) > 1),'un',0);
    end

    % Set 'k'-gram for usage in fprintf
    % For example, unigram has only one character, so unit would be '%s' for
    % psv. Trigram has three characters, so unit would be '%s%s%s' for psv.
    unit = [];
    for j = 1:n
        unit = [unit, '%s'];
    end
    
    % Set attribute class vector; for example, unigram has {T,C} for
    % T/C bases library.
    red_vocab = vocab(find(~(contains(vocab,'G')+contains(vocab,'A'))));
    % Attribute class vector in a single line
    att_line = [];
    for p = 1:length(red_vocab)-1
        att_line = [att_line, char(red_vocab(p)),','];
    end
    att_line = [att_line, char(red_vocab(p+1))];
    
    % Save the feature vector with a proper WEKA input format
    fid = fopen(saveFile,'w');
    % Feature vector description
    fprintf(fid, ['@RELATION TC_',fvtype,'_%dchar  \n\n'], n);
    % Attribute type and class
    for m = 1:(l_seq(1)-n+1)
        fprintf(fid, ['@ATTRIBUTE	pos%d    {',att_line,'}  \n'],m);
    end
    fprintf(fid, '@ATTRIBUTE	class	{Y,N}  \n');
    % Feature vector data
    fprintf(fid, '\n@DATA\n');
    for i = 1:size(seq,1)
        for j = 1:(l_seq(1)-n+1)
            fprintf(fid, [unit, ','], seq_TC_char{i,j:j+n-1});
        end
        fprintf(fid,'%s\n',class{i,1});
    end
    fclose(fid)
end


if fvtype == 'tfv'
    % Count number of each n-gram in a given sequence
    [counts, class, l_seq] = seq2count(readFile,n);
    % Calculate frequency
    freq = counts./(l_seq(1)-n+1);
    
    % Find index for corresponding library (T/C library in this study)
    vocabIdx = find(~(contains(vocab,'G')+contains(vocab,'A')));
    % Save the feature vector with a proper WEKA input format
    fid = fopen(saveFile,'w');
    % Feature vector description
    fprintf(fid, ['@RELATION TC_' fvtype '_%dchar  \n\n'], n);
    % Attribute type and class
    for m = 1:length(vocabIdx)
        fprintf(fid, '@ATTRIBUTE	FREQ_%s REAL \n',char(vocab(vocabIdx(m))));
    end
    fprintf(fid, '@ATTRIBUTE	class	{Y,N}  \n');
    % Feature vector data
    fprintf(fid, '\n@DATA\n');
    for i = 1:size(freq,1)
        for j = 1:length(vocabIdx)
            fprintf(fid, '%f,', freq(i,vocabIdx(j)));
        end
        fprintf(fid,'%s\n',class{i,1});
    end
    fclose(fid)
end








