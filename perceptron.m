function y = perceptron(w, x, b)
% Perform as a single perceptron. It maps its input x (a real-valued vector) 
% to an output value y (a single binary value) using sigmoid function as
% activation function.
% 
% Input variables:
% w: weight
% x: input signal
% b: bias

for i = 1:length(w)
    unity(i,1) = w(i)*x(i);
end
y = sigmoid(sum(unity)+b);


