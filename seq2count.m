function [counts, class, l_seq] = seq2count(readFile,k)

if size(readFile, 1) == 1
    rawText = textread(readFile, '%s', 'whitespace',',');
    seq = rawText(1:2:length(rawText));
    class = rawText(2:2:length(rawText));
else
    seq = readFile(:,1);
    class = readFile(:,2);
end

% number of the sequence examples
numLines = length(seq);
% sequence length 
l_seq = zeros(numLines,1);


% convert sequence information to frequency vector                
vocab = genVocab(k);
counts = zeros(numLines,length(vocab));

for i = 1:numLines
    thisLine = cell2mat(seq(i));
    for j = 1:length(vocab)
        counts(i,j) = length(strfind(thisLine, vocab(j)));
    end
    l_seq(i) = sum(counts(i,:)) + k-1;
end




