function [motif, prob, seq_idx_pos, seq_idx_neg]=extract_motif(readFile, N_pos, N_neg)
% This function loads a results file of MERCI (*.occurrences) then return
% the motif, thr probability and the sequence index that each motif
% occurs.ei

% Input variables:
% readFile: input filename (*.occurence) 
% N_pos: the number of positive examples
% N_neg: the number of negative examples


% Load *.occurrences file
rawText = textread(readFile, '%s', 'whitespace',',');

% Find line index to extract the motif and sequences
% idx_pos returns positive sequence index that a given motif occurs
% idx_neg returns negative sequence index that a given motif occurs
idx_motif = find(cellfun('length',regexp(rawText,'MOTIF:')) == 1);
idx_pos = find(cellfun('length',regexp(rawText,'positive')) == 1);
idx_neg = find(cellfun('length',regexp(rawText,'negative')) == 1);

% Initialize variables
temp_motif = [];
k = 1;
% Extract motifs, sequences, and how many sequences occur for a given motif
for i = 1:size(idx_motif,1)
    line_motif = strsplit(rawText{idx_motif(i)});
    line_pos = strsplit(rawText{idx_pos(i)});
    line_neg = strsplit(rawText{idx_neg(i)});
    for j = 2:size(line_motif,2)
        temp_motif = [temp_motif line_motif{j}];
    end
    if isempty(strfind(temp_motif,'gap'))
        motif{k,:} = temp_motif;
    else
        motif{k,:} = strrep(temp_motif,'gap','-');
    end

    temp_motif = [];
    count_pos(k,1) = str2num(line_pos{1});
    count_neg(k,1) = str2num(line_neg{1});
    k = k+1;
end


% conditional probability of 'Y' given 'motif'
% P(Y|motif) = P(Y and motif)/P(motif)
for i = 1:size(count_pos,1)
    P_motif(i,1) = (count_pos(i) + count_neg(i))/(N_pos + N_neg);
    P_Ymotif(i,1) = count_pos(i)/(N_pos + N_neg);
end

prob = P_Ymotif./P_motif;

% Find sequence index that a given motif occurs
for i = 1:size(idx_motif,1)
    p = 1;
    q = 1;
    if i < size(idx_motif,1)
        for k = idx_motif(i):idx_motif(i+1)-1
            line_class = strsplit(rawText{k});
            if cellfun('length',regexp(line_class,'rec')) == 1 & cellfun('length',regexp(line_class,'nonrec')) ~= 1
                temp = strsplit(rawText{k+1});
                temp_num = temp{2};
                idx_num = strfind(temp_num,')');
                seq_idx_pos(p,i) = sscanf(temp_num(1:idx_num-1), '%g', 1);
                p = p+1;
            elseif cellfun('length',regexp(line_class,'nonrec')) == 1
                temp = strsplit(rawText{k+1});
                temp_num = temp{2};
                idx_num = strfind(temp_num,')');
                seq_idx_neg(q,i) = sscanf(temp_num(1:idx_num-1), '%g', 1);
                q = q+1;
            end
        end
    else
        for k = idx_motif(i):idx_neg(i)
            line_class = strsplit(rawText{k});
            if cellfun('length',regexp(line_class,'rec')) == 1 & cellfun('length',regexp(line_class,'nonrec')) ~= 1
                temp = strsplit(rawText{k+1});
                temp_num = temp{2};
                idx_num = strfind(temp_num,')');
                seq_idx_pos(p,i) = sscanf(temp_num(1:idx_num-1), '%g', 1);
                p = p+1;
            elseif cellfun('length',regexp(line_class,'nonrec')) == 1
                temp = strsplit(rawText{k+1});
                temp_num = temp{2};
                idx_num = strfind(temp_num,')');
                seq_idx_neg(q,i) = sscanf(temp_num(1:idx_num-1), '%g', 1);
                q = q+1;
            end
        end
    end
end
                
            
        
