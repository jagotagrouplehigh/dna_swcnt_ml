function segment = create_segments(readFile,m)
% This function import a csv file of sequences set, then create m number of
% segments. The length of each segments n = l_seq/m where l_seq is the 
% sequence length. In this script, N should be a multiple of m. 

%% Load the sequence set
if size(readFile, 1) == 1
    % Import the sequence set from csv file
    rawText = textread(readFile, '%s', 'whitespace',',');
    % Sequence
    seq = rawText(1:2:length(rawText));
    % Class
    class = rawText(2:2:length(rawText));
else
    % Sequence
    seq = readFile(:,1);
    % Class
    class = readFile(:,2);
end

% sequence total length
l_seq = length(seq{1});
% number of examples
numLines = length(seq);

%% Create segments
% length of segment
n = floor(l_seq/m);

% Create sequences
for i = 1:numLines
    temp = seq{i,1};
    for j = 1:m
        if j == m && mod(l_seq,m) > 0
            segment{i,j} = temp(1+n*(j-1):l_seq);
        else
            segment{i,j} = temp(1+n*(j-1):j*n);
        end
    end
end

% Put class in last column
segment(:,m+1) = class;
