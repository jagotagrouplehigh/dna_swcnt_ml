function [ndout, ndhd, Threshold]=extract_weight(readFile,Nh,lfv)
% This function read the WEKA ANN result file and extract the weight.

% Input variables:
% readFile: input filename
% lfv: length of feature vector
% Nh: number of hidden units

% Read the WEKA ANN result file
rawText = textread(readFile, '%s', 'whitespace',',');

% Find line index where weight information is located
idx_start = find(cellfun('length',regexp(rawText,'Weights')) == 1);

k = 1;
% Extract the lines which have weight information using idx_start
for i = 1:2
    for j = 1:Nh
        data(k,:) = strsplit(rawText{idx_start(i)+j+1,1});
        k = size(data,1) + 1;
    end
end
for i = 3:Nh+2
    for j = 1:lfv
        data(k,:) = strsplit(rawText{idx_start(i)+j+1,1});
        k = size(data,1) + 1;
    end
end

% Extract the threshold values
for i = 1:Nh+2
    Thrs(i,:) = strsplit(rawText{idx_start(i)+1,1});
end
Threshold = cellfun(@(x)str2double(x), Thrs(:,2));

% Extract the weights for output nodes
for i = 1:2
    nodes_out(:,i) = data(Nh*(i-1)+1:Nh*i,3);
end
ndout = cellfun(@(x)str2double(x), nodes_out);

% Extract the weights for hidden nodes
for i = 1:Nh
    nodes_hd(:,i) = data(Nh*2+lfv*(i-1)+1:Nh*2+lfv*i,3);
end
ndhd = cellfun(@(x)str2double(x), nodes_hd);
    
