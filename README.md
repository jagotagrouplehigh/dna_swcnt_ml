# Prediction of recognition DNA sequence of SWCNT

Code and files presented in this repository are for reproducing the input feature vector construction and saliency analysis in [Y. Yang, M. Zheng & A. Jagota (2019)](https://doi.org/10.1038/s41524-018-0142-3).

Note that all codes (\*.m) in the repository is written in MATLAB. 
'allcomb' function is required to implement this work. The 'allcomb' function can be downloaded from [here](https://www.mathworks.com/matlabcentral/fileexchange/10064-allcomb-varargin).


## Input feature construction
Convert DNA sequence or motif analysis results by MERCI into numeric feature vector. Note that the created feature vector files were saved in **trainingSet** directory.

- Sample sequence set is provided: 
	- `seq_TC_12mer.csv` contains DNA sequences and classes for initial training set 
	- `seq_TC_12mer_1strun.csv` contains DNA sequences and classes for for 1st re-trained training set (initial training set + 10 newly tested sequences)


- `save_seq2fv.m` reads a csv file of the sequence sets then convert to the position specific vector (psv) or term frequency vector (tfv) with overlapping _n_-gram.
	- `genVocab.m` returns the _n_-gram volubulary vector of T/C/A/G combination.
	- `seq2count.m` counts number of each n-gram in a given sequence.
	
- Example of DNA sequence to tfv:

```Matlab
>>> n = 3;										% set n-gram
>>> fvtype = 'tfv';								% set feature vector type; 'psv' or 'tfv'
>>> readFile = './seq_TC_12mer.csv'				% inputFile name
>>> saveFile = './trainingSet/TC_tfv_3gram.csv'	% saveFile name
>>> save_seq2fv(readFile, n, fvtype, saveFile)	% run 'save_seq2fv.m'
```


- `save_seq2segmentedtfv.m` reads a csv file of the sequence sets then convert DNA sequence to segmented term frequency vector (segemented-tfv).
	- `create_segments.m` import a csv file of sequences set, then create m number of segments.
	- `extract_counts_intersection.m` loads segments set, then count _n_-gram in either segment and take a weighted average of their occupancy.

- `save_seq2combinedtfv.m` reads csv files of multiple _n_-gram term frequency vector sets then reformats to single combined term frequency vector (combined-tfv).

- `save_motif2fv.m` reads a results file of MERCI (\*.occurrences) then converts to the motif-based feature vector (mfv).
	- `saveasfasta_eachclass.m` returns two separated fasta format txt files that include only positive (recognition) or negative (non-recognition) sequences.
		This generates `TC12_pos.txt` and `TC12_neg.txt`.
	
	- `extract_motif.m` reads a MERCI result file (\*.occurrences) then extracts motifs, their probability, and sequence index that each motif occurs. 
		Sample MERCI results files are given in **MERCIresults** directory. 
		
- Example of motif to mfv (the maximal length of positive motif is 7 and negative motif is 5):
```Matlab
>>> l_motif_pos = 7;					% maximal length of positive motif when MERCI discovered motifs
>>> l_motif_neg = 5;					% maximal length of negative motif when MERCI discovered motifs
>>> Nmotif = 10;						% number of motifs to use in feature vector
>>> save_motif2fv(l_motif_pos,l_motif_neg,Nmotif)	% run 'save_motif2fv.m'
```

## Generate DNA sequences
Generate all 12-mer sequences with a given base set excluding sequences in the training set.

- `main_allseq.m` is the main script to generate all sequences with a given base set and set class as '?'. The sequences will be used to predict new recognition sequences.
	- `gen_allseq.m` generates all the possible 12-mer sequences then exclude overlapped sequences that appear in the training set. 

## Combine multiple models by cross-validation 
Combine multiple models by cross-validation and select the sequences only from the intersection of each set of classifier results.  

- `ComparePredictionResults.m` reads predicted sequences sets by multiple classifier then find the consensus sequence within multiple models. 

## Saliency analysis
Calculate derivative-based saliency measures (developed by [Ruck et. al.](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.31.6617)).

- `saliency_results.m` is the main function for saliency analysis. It reads the training set (feature vector) and WEKA ANN result file, calculates the saliency then returns average and standard deviation of normalized saliecy for a given model.
	- `extract_freq.m` extracts the term frequecy from the WEKA input format csv file.
	- `extract_weight.m` extracts the weights and biases from WEKA results file for a given ANN model.
	- `perceptron.m` performs as a single perceptron. It maps its input x (a real-valued vector) to an output value y (a single binary value) using sigmoid function as activation function.
	- `sigmoid.m` returns outputs of sigmoid function. 

- Example of saliency calucation with 3-gram tfv:
```Matlab
>>> featureType = 'tfv3';				% feature vector type (psv/tfv/mfv/segmentedtfv/combinedtfv)
>>> trFile = 'TC_tfv_3gram';			% Training set filename
>>> lfv = 2^3;							% length of feature vector (2^n for n-gram tfv)
>>> Nh = 11;							% number of nodes in a hidden layer in a given ANN model
>>> [norm_sal_mean, norm_sal_std]= saliency_results(featureType,trFile, lfv, Nh)	% run 'saliency_results.m'
```
