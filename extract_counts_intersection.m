function counts = extract_counts_intersection(segment,m,j)
% This function loads segments set, then count n-gram in either segment 
% and take a weighted average of their occupancy.

% Show the error if input is for unigram (j=1).
if j < 2
    error('Error. Term frequency vector must be higher than unigram.')
end

% number of examples
numLines = size(segment,1);
% length of segment
n = length(segment{1});

%% Extract the straddle segments
temp = [];
q = 1;
for i = 1:numLines
    for k = 1:m
        temp = [temp segment{i,k}];
    end
    for k = 1:m-1
        for p = j-1:-1:1
            seg_int{i,q} = temp(n*k-(p-1):n*k-(p-1)+j-1);
            q = q+1;
        end
    end
    temp = [];
    q = 1;
end


%% Count number of n-gram in each straddle segment
% Generate j-gram feature vocabulary
features = genVocab(j);
% Initialize variable
counts = zeros(numLines,length(features));

% Count number of n-gram in each segment
for t = 1:size(seg_int,2)
    for i = 1:numLines
        thisLine = cell2mat(seg_int(i,t));
        for j = 1:length(features)
            counts(i,j,t) = length(strfind(thisLine, features(j)));
        end
    end
end
