function vocab = genVocab(k)
% This function returns the k-gram volubulary vector of T/C/A/G combination.
% 'allcomb' function is required to run this function.
% 'allcomb' function can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/10064-allcomb-varargin

% Set base vector
baseset = char(['T','A','C','G']);
% Make a numeric vector with the same size of the baseset vector to input
% allcomb function.
num = [1:length(baseset)];    

% Generate the vocabulary vector of T,C,A,G combination (valid only for
% unigram to 5-gram)
if k == 1
    vocabNum = allcomb(num);
elseif k == 2
    vocabNum = allcomb(num,num);
elseif k == 3
    vocabNum = allcomb(num,num,num);
elseif k == 4
    vocabNum = allcomb(num,num,num,num);
elseif k == 5
    vocabNum = allcomb(num,num,num,num,num);
end

% 
for i = 1:length(vocabNum)
    voc_seq{1,i} = baseset(vocabNum(i,:));
end
vocab = string(voc_seq);