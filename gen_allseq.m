function allSeq = gen_allseq(baseSet, trainingSeq)
% This function generates all the possible 12mer sequences
% 'allcomb' function is required to run this function.
% 'allcomb' function can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/10064-allcomb-varargin


% Convert character vector to numeric vector
a = [1:length(baseSet)];
% Fine all combination of 12-mer with given base set
allComb = allcomb(a,a,a,a,a,a,a,a,a,a,a,a);

j = 1;
% Get rid of overlapped sequences (sequences in training set)
for i = 1:length(allComb)
    % Convert an numeric vector to a sequence
    seq = baseSet(allComb(i,:));
    % Find overlapped sequence and if there is no overlapped sequence,
    if any(cellfun('length',regexp(trainingSeq,seq))) == 0
        allSeq(j,1) = cellstr(seq);
        j = j+1;
    end
end
    


