function [freq class_trained] =extract_freq(readFile,lfv)
% This function read the WEKA input format file and extract the frequency

% Input variables:
% readFile: input filename
% lfv: length of feature vector

% Read file
rawText = textread(readFile, '%s', 'whitespace',',');

% Data starts after '@DATA'
idx_start = find(cellfun('length',regexp(rawText,'@DATA')) == 1);

j = 1;
for i = idx_start+1:lfv+1:length(rawText)
    freq_cell(j,1:lfv) = rawText(i:i+lfv-1);
    class_trained(j,1) = rawText(i+lfv);
    j = j+1;
end

% Convert cell to numeric matrix
freq = cellfun(@(x)str2double(x), freq_cell);