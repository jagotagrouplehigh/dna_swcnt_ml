function save_seq2combinedtfv(nset)
% Convert individual term frequency vector sets to combined term frequency 
% vector (combined-tfv)

% nset : n-gram set to combine (e.g., nset = [1,2,3] for combined unigram,
% bigram, and trigram 

% Example: combined bigram-trigram term frequency vector, combined_2_3
% nset = [2 3];
% save_seq2combinedtfv(nset);


%% Combine multiple term frequency vectors
path_tr = './trainingSet/';
p = 1;
for k = 1:size(nset,2)
    n = nset(k);
    
    % Load each n-gram frequency from csv file
    readFile = [path_tr 'TC_tfv_' num2str(n) 'gram.csv'];
    rawText = textread(readFile, '%s', 'whitespace',',');
    
    % Data starts after '@DATA'
    idx_start = find(cellfun('length',regexp(rawText,'@DATA')) == 1);
    
    j = 1;
    for i = idx_start+1:2^n+1:length(rawText)
        freq_cell(j,p:p+2^n-1) = rawText(i:i+2^n-1);
        class_trained(j,k) = rawText(i+2^n);
        j = j+1;
    end
    p = p + 2^n;
end
% Convert cell to numeric matrix
freq = cellfun(@(x)str2double(x), freq_cell);

%% save feature matrix as csv
% Set filename to save (saveFile: basetypes_combined_n1gram_n2gram_..._nkgram 
% for nset = [n1, n2, ..., nk]) 
saveFile = [path_tr 'TC_combined'];
for i = 1:size(nset,2)
     saveFile = horzcat(saveFile, ['_' num2str(nset(i))]);
end

% Save the feature vector with a proper WEKA input format
fid = fopen([saveFile '.csv'],'w');
% Feature vector description
fprintf(fid, ['@RELATION' saveFile '\n\n']);
% Initialize variable
features = [];
% Set combined feature types
for i = 1:size(nset,2)
    vocab = genVocab(nset(i));
    % Set attribute class vector; for example, unigram has {T,C} for
    % T/C bases library.
    red_vocab = vocab(find(~(contains(vocab,'G')+contains(vocab,'A'))));
    features = [features red_vocab];
end
% Feature types and class
for m = 1:length(features)
    fprintf(fid, '@ATTRIBUTE	FREQ_%s	REAL  \n',features(m));
end
fprintf(fid, '@ATTRIBUTE	class	{Y,N}  \n');
% Feature vector data
fprintf(fid, '\n@DATA\n');

for i = 1:size(freq,1)
    for j = 1:size(freq,2)
        fprintf(fid,'%f,', freq(i,j));
    end
    fprintf(fid,'%s\n',class_trained{i,1});
end
fclose(fid);
    
    