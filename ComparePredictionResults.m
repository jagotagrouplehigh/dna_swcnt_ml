clc
clear all
close all

% Set filename to save
saveFile = 'seq_totest_TC12_1strun';
% Read all sequences which was used to predict sequences
readFile = 'testTC_12mer_1strun.csv';
rawText = textread(readFile, '%s', 'whitespace',',');
seq = rawText(1:2:length(rawText));

% Set models for cross-validation
modeltype = {'LR',  'psv1',    '1p1'; ...
             'LR',  'psv2',    '3'; ...
             'LR',  'psv3',    '7'; ...
             'LR',  'tfv2',    '1p5'; ...
             'LR',  'tfv3',    '1'; ...
             'ANN', 'psv1',    '1'; ...
             'ANN', 'psv2',    '1p7'; ...
             'ANN', 'psv3',    '2p2'; ...
             'ANN', 'tfv2',    '1'; ...
             'ANN', 'tfv3',    '1'; ...
             'SVM', 'psv2',    '1'; ...
             'SVM', 'psv3',    '2'};
         
% Extract predicted class and prediction probability
for i = 1:size(modeltype,1)
    featureType = char(modeltype(i,2));
    costFactor = char(modeltype(i,3));
    algthm = char(modeltype(i,1));
    filename = ['./predictionSet/TC_' algthm '_' featureType '_cost' costFactor];
    
    %  Read prediction result file
    fid = fopen([filename '.csv'], 'r');
    tline = fgetl(fid);
    ctr = 0;
    while(~feof(fid))
        if ischar(tline)
            ctr = ctr + 1;
            tline = fgetl(fid);
            temp(ctr,:) = regexp(tline, '\,', 'split');
        else
            break;
        end
    end
    fclose(fid);
    
    class(:,i) = temp(:,3);
    prob_cell = temp(:,5);
    prob(:,i) = cellfun(@(x)str2double(x), prob_cell);
    % number of examples labeled as 'Y'
    N_yes(1,i) = sum(count(class(:,i),'Y'));
end

% N_seq: number of all predicted sequences
% N_set: number of models
[N_seq, N_model] = size(class);

% Extract any sequences predicted as 'Y' label and their index
k = 1;
idx_pred = 0;
for i = 1:N_seq
    if any(~cellfun('isempty',strfind(class(i,:),'Y'))) == 1
        seq_pred(k,1) = seq(i,1);
        idx_pred(k,1) = i;
        k = k+1;
    end
end

% Find consensus sequences
if idx_pred ~= 0        % If there is one or more sequences predicted as 'Y'
    % Count how many a given sequence is predicted as 'Y' label
    for i = 1:length(idx_pred)
        count_Y(i,1) = N_model - ...
            sum(cellfun('isempty',strfind(class(idx_pred(i),:),'Y')));
    end
    
    % Initialize variables
    m = 1;
    seq_totest = {'0'};
    % Extract consensus sequence within all models
    for i = 1:length(idx_pred)
        if count_Y(i,1) >= N_model
            seq_totest(m,1) = seq_pred(i);
            m = m+1;
        end
    end
    
    % If there is one or more consensus sequences, save the sequence set
    if seq_totest{1} ~= '0'
        fid = fopen([saveFile '.csv'],'w');
        for i = 1:length(seq_totest)
            fprintf(fid,'%s\n',seq_totest{i,1});
        end
        fclose(fid);
    end
end
