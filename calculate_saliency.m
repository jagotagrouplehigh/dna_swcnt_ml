function norm_sal = calculate_saliency(freq, Nh, lfv, ndout, ndhd, bias)
% This function calculate the saliency for a given ANN model.
% 
% Input variables:
% freq: training feature vector set
% lfv: length of feature vector
% It will be 2^n for n-gram tfv, (l_seq-n+1) for n-gram psv, 2*Nmotif for
% mfv, (2^n)*m for segmentedtfv Sm_n, sum(2^n1, 2^n2, ..., 2^nk) for 
% combinedtfv with nset = [n1, n2, ..., nk]
% Nh: number of nodes in a hidden layer
% ndout: weights of output nodes
% ndhd: weights of hidden nodes
% bias

% normalize features to [-1 1]
for j = 1:lfv
    for i = 1:size(freq,1)
        maxfreq = max(freq(:,j));
        minfreq = min(freq(:,j));
        freq_norm(i,j) = 2*(freq(i,j) - minfreq)/(maxfreq - minfreq)-1;
    end
end

% Calculate output from 1st hidden layer
for j = 1:size(freq,1)
    for i = 1:Nh
        x1(i,j) = perceptron(ndhd(:,i),freq_norm(j,:)',bias(i+2,1));
    end
end


% Calculate output from output layer
for j = 1:size(freq,1)
    for i = 1:2
        z(j,i) = perceptron(ndout(:,i),x1(:,j),bias(i,1));
    end
end

%% Calculate saliency

% Arbitrary set of R points for input x which will be sampled (R is set to 30)
k = 1;
freq_norm_prime_temp = freq_norm;
for m = 1:lfv    
    for n = 0:1/30:1
        freq_norm_prime_temp(:,:,k) = freq_norm;
        for j = 1:size(freq,1)
            freq_norm_prime_temp(j,m,k) = n;
        end
        k = k+1;
    end
    if m == 1
        freq_norm_prime = freq_norm_prime_temp;
    else
        freq_norm_prime = [freq_norm_prime; freq_norm_prime_temp];
    end
    k = 1;
end

% Output values from a hidden layer with the arbitrary set + original set 
% as input 
for k = 1:size(freq_norm_prime,3)
    for j = 1:size(freq,1)
        for i = 1:Nh
            x1_prime(i,j,k) = perceptron(ndhd(:,i),freq_norm_prime(j,:,k)', ...
                bias(i+2,1));
        end
    end
end

% Initialize variable
dzdx = zeros(size(freq,1),lfv);

% Calculate the saliency
for j = 1:lfv
    for i = 1:size(freq,1)
        for k = 1:size(freq_norm_prime,3)
            for m = 1:Nh
                dzdx(i,j) = dzdx(i,j) + z(i,1)*(1-z(i,1))*ndout(m,1)* ...
                    x1_prime(m,i,k)*(1-x1_prime(m,i,k))*ndhd(j,m);
            end
        end
    end
end

saliency = sum(dzdx,1);
% Normalize saliency
norm_sal = saliency/(size(freq_norm_prime,1)*size(freq_norm_prime,3)/4);


