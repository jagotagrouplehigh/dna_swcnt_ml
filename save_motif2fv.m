function save_motif2fv(l_motif_pos, l_motif_neg, Nmotif)
% This function loads a results file of MERCI (*.occurrences) then convert
% to the motif-based feature vector (mfv) and save the feature vector as
% arff format for WEKA.  
% Input variables:
% l_motif_pos: the maximal length of positive motif when MERCI discovered motifs
% l_motif_neg: the maximal length of negative motif when MERCI discovered motifs
% Nmotif : the number of motifs to use in feature vector
% 
% Example: the maximal length of positive motif is 7 and negative motif is 
% 5. Each motif set has 10 motifs. 
% l_motif_pos = 7;
% l_motif_neg = 5;
% Nmotif = 10;
% save_motif2fv(l_motif_pos,l_motif_neg,Nmotif)

% Load each recognition and non-recognition sequences from FASTA files
[head,seq_rec] = fastaread('./TC12_pos.txt');
[head,seq_non] = fastaread('./TC12_neg.txt');

% total number of positive(recognition)/negative(non-recognition) sequences
N_pos = length(seq_rec);
N_neg = length(seq_non);

%% positive motif
% Load MERCI results file (*.occurrences) of positive motifs
filename = ['TC12_pos_motif_l' num2str(l_motif_pos)];
readFile = ['./MERCIresults/' filename '.occurrences'];

% Extract motifs, their probability, and sequence index that each motif occurs
[motif, prob, seq_idx_pos, seq_idx_neg] = extract_motif(readFile, N_pos, N_neg);

% Sort probability array in descending order
[prob_des, order] = sort(prob, 'descend');

% Take Nmotif number of motifs then sort corresponding sequence index in 
% descending order.
seq_idx_pos_des = seq_idx_pos(:,order(1:Nmotif));
seq_idx_neg_des = seq_idx_neg(:,order(1:Nmotif));
motif_pos_des = motif(order(1:Nmotif));

% The motifs were encoded as 2*Nmotif-dimensional binary feature vector, mfv.
% Entry m is set to ?1? if motif m occurs in a given sequence and ?0? otherwise. 
motif_fv = zeros(N_pos+N_neg,Nmotif*2);
for i = 1:Nmotif
    for j = 1:size(seq_idx_pos_des,1)
        if seq_idx_pos_des(j,i) ~= 0
            motif_fv(seq_idx_pos_des(j,i),i) = 1;
        end
    end
    for k = 1:size(seq_idx_neg_des,1)
        if seq_idx_neg_des(k,i) ~= 0
            motif_fv(seq_idx_neg_des(k,i)+N_pos,i) = 1;
        end
    end
end
    
    
%% negative motif
% Load MERCI results file (*.occurrences) of positive motifs
filename = ['TC12_neg_motif_l' num2str(l_motif_neg)];
readFile = ['./MERCIresults/' filename '.occurrences'];

% Extract motifs, their probability, and sequence index that each motif occurs
[motif_neg, prob_neg, seq_idx_pos2, seq_idx_neg2]=extract_motif(readFile, N_pos, N_neg);

% Sort probability array in descending order
[prob_neg_des, order_neg] = sort(prob_neg, 'descend');

% Take Nmotif number of motifs then sort corresponding sequence index in 
% descending order.
seq_idx_pos_des2 = seq_idx_pos2(:,order_neg(1:Nmotif));
seq_idx_neg_des2 = seq_idx_neg2(:,order_neg(1:Nmotif));
motif_neg_des = motif_neg(order_neg(1:Nmotif));

% Entry m is set to ?1? if motif m occurs in a given sequence and ?0? otherwise. 
for i = 1:Nmotif
    for j = 1:size(seq_idx_pos_des2,1)
        if seq_idx_pos_des2(j,i) ~= 0
            motif_fv(seq_idx_pos_des2(j,i),i+Nmotif) = 1;
        end
    end
    for k = 1:size(seq_idx_neg_des2,1)
        if seq_idx_neg_des2(k,i) ~= 0
            motif_fv(seq_idx_neg_des2(k,i)+N_pos,i+Nmotif) = 1;
        end
    end
end


%% save feature vector as csv file
% Set filename to save 
% (saveFile: basetypes_mfv_Nmotif_lpos(l_motif_pos)_Nmotif_lneg(l_motif_neg)) 
path_tr = './trainingSet/';
saveFile = [path_tr 'TC_mfv_' num2str(Nmotif) '_lpos' num2str(l_motif_pos) ...
            '_' num2str(Nmotif) '_lneg' num2str(l_motif_neg)];

% Save the feature vector with a proper WEKA input format
fid = fopen([saveFile '.csv'],'w');
% Feature vector description
fprintf(fid, ['@RELATION ' saveFile ' \n\n']);
% Attribute type and class
for m = 1:Nmotif
    fprintf(fid, '@ATTRIBUTE	pos_motif_%s	REAL  \n',string(motif_pos_des(m)));
end
for m = 1:Nmotif
    fprintf(fid, '@ATTRIBUTE	neg_motif_%s	REAL  \n',string(motif_neg_des(m)));
end
fprintf(fid, '@ATTRIBUTE	class	{Y,N}  \n');
% Feature vector data
fprintf(fid, '\n@DATA\n');
for i = 1:N_pos
    for j = 1:2*Nmotif
        fprintf(fid,'%s,', num2str(motif_fv(i,j)));
    end
    fprintf(fid,'%s\n','Y');
end
for i = N_pos+1:N_pos+N_neg
    for j = 1:2*Nmotif
        fprintf(fid,'%s,', num2str(motif_fv(i,j)));
    end
    fprintf(fid,'%s\n','N');
end
fclose(fid);