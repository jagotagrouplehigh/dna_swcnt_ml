% Generate all sequences excluding sequences in the training set
% 
% 'allcomb' function is required to run this function.
% 'allcomb' function can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/10064-allcomb-varargin

clear all

readFile = './seq_TC_12mer_1strun.csv';
rawText = textread(readFile, '%s', 'whitespace',',');
trainingSeq = rawText(1:2:length(rawText));
class = rawText(2:2:length(rawText));

baseSet = char(['C' 'T']);

allSeq = gen_allseq(baseSet, trainingSeq);

for i = 1:size(allSeq,1)
    testset(i,:) = [allSeq(i,1),'?'];
end

%% Example: Save as csv file with trigram (n=3) position specific vector (psv)
n = 3;
fvtype = 'psv';
saveFile = ['testTC_' fvtype '_' num2str(n) 'gram.csv'];
save_seq2fv(testset,n,fvtype,saveFile)

