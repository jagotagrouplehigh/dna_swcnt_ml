function [norm_sal_mean, norm_sal_std]= saliency_results(featureType,trFile, lfv, Nh)
% This function read the training set and WEKA ANN result file, calculate
% saliency then return average and standard deviation of saliecy for a
% given model.
% 
% Input variables:
% featureType: psv/tfv/mfv/segmentedtfv/combinedtfv
% trFile: Training set filename
% lfv: length of feature vector
% It will be 2^n for n-gram tfv, (l_seq-n+1) for n-gram psv, 2*Nmotif for
% mfv, (2^n)*m for segmentedtfv Sm_n, sum(2^n1, 2^n2, ..., 2^nk) for 
% combinedtfv with nset = [n1, n2, ..., nk]
% Nh: number of nodes in a hidden layer
% 
% Example: saliency of ANN with trigram term frequency vector
% featureType = 'tfv3';
% trFile = 'TC_tfv_3gram';
% lfv = 2^3;
% Nh = 11;
% [norm_sal_mean, norm_sal_std]= saliency_results(featureType,trFile, lfv, Nh)


path = './WEKAresults/';
path_tr = './trainingSet/';


%% read required data and calculate saliencies

% Load training set and extract frequency
readFile_tr = [path_tr trFile '.csv'];
[freq class_trained] = extract_freq(readFile_tr,lfv);

% List all result filenames which is implemented with different seed number 
% All results files are sitting in a given directory
fid = dir([path featureType '/' trFile '_seed*']);
fileNames = {fid.name};
% Calculate saliencies
for p = 1:size(fileNames,2)
    % WEKA model result filename in a given directory
    readFile = [path featureType '/' char(fileNames(p))];
    % Extract the values of bias, weights of hidden nodes (ndhd), and
    % weights of output nodes (ndout)
    [ndout, ndhd, bias]=extract_weight(readFile,Nh,lfv);
    % Calculate saliencies
    norm_sal(p,:) = calculate_saliency(freq, Nh, lfv, ndout, ndhd, bias);
end


%% calculate mean and std
% average of normalized saliency of ANN models with random seeds
norm_sal_mean = mean(norm_sal,1);
% standard deviation of normalized saliency of ANN models with random seeds
norm_sal_std = std(norm_sal,1);

% Plot the average of normalized saliency with std
figure (1)
bar(norm_sal_mean)
hold on
errorbar(norm_sal_mean, norm_sal_std, '.')
hold off
