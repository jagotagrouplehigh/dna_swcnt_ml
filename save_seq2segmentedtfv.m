function save_seq2segmentedtfv(readFile,m,j,saveFile)
% Convert sequence set to segmented term frequency vector (segemented-tfv)
% Contribution to the tfv from terms that straddle segment boundaries are 
% made according to a weighted average of their occupancy in either segment.  
% 
% Input variables:
% m : # of segments
% N : sequence length (not used in this code)
% n : length of each segment (N/m), if N is not a multiple of m, make the
% last segment (n+1)-base long. (not used in this code)
% j : # of characters in one feature (j-gram)
% Sm_j : sequence encoding with m segments and j-gram
% 
% 'allcomb' function is required to run this function.
% 'allcomb' function can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/10064-allcomb-varargin
% 
% Example: 4 segments trigram term frequency vector, S4_3
% m = 4;
% j = 3;
% readFile = './seq_TC_12mer.csv';
% path_tr = './trainingSet/';
% saveFile = [path_tr 'TC_S' num2str(m) '_' num2str(j) '.csv'];
% save_seq2segmentedtfv(readFile,m,j,saveFile);

%% Convert sequence set to segmented tfv
% Create segment set
segment = create_segments(readFile,m);

% Count the frequency of n-gram in each segment
for p = 1:m
    [temp_ct, class, l_seq] = seq2count([segment(:,p) segment(:,m+1)],j);
    counts(:,:,p) = temp_ct;
end

% Straddle segment boundaries:
% Count the n-gram in either segment and take a weighted average of their 
% occupancy.
if j > 1
    counts_inter = extract_counts_intersection(segment,m,j);
    % Take a weighted average
    for q = 1:m
        for s = 1:j-1
            if q == 1
                counts(:,:,1) = counts(:,:,1) + counts_inter(:,:,s)*(j-s)/j;
            elseif q == m
                counts(:,:,m) = counts(:,:,m) + counts_inter(:,:,(m-2)*(j-1)+s)*s/j;
            else
                counts(:,:,q) = counts(:,:,q) + counts_inter(:,:,(q-2)*(j-1)+s)*s/j;
                counts(:,:,q) = counts(:,:,q) + counts_inter(:,:,(q-2)*(j-1)+s+(j-1))*(j-s)/j;
            end
        end
    end
    
end

vocab = genVocab(j);
% Find index for corresponding library (T/C library in this study)
vocabIdx = find(~(contains(vocab,'G')+contains(vocab,'A')));

% Resclae features for each segment
for q = 1:m
    denom(q,1) = sum(counts(1,:,q),2);
    for i = 1:size(segment,1)
        for p = 1:2^j
            counts_TC(i,p,q) = counts(i,vocabIdx(p),q);
            freq(i,p,q) = counts_TC(i,p,q)/denom(q,1);
        end
    end
end

%% save files as .csv format

% Save the feature vector with a proper WEKA input format
fid = fopen(saveFile,'w');
% Feature vector description
fprintf(fid, ['@RELATION TC_S' num2str(m) '_' num2str(j) '\n\n']);
% Attribute type and class
for i = 1:m
    for p = 1:2^j
        fprintf(fid, '@ATTRIBUTE	FREQ_S%d_%s	REAL  \n',i,vocab(vocabIdx(p)));
    end
end
fprintf(fid, '@ATTRIBUTE	class	{Y,N}  \n');
% Feature vector data
fprintf(fid, '\n@DATA\n');
for i = 1:size(segment,1)
    for q = 1:m
        for p = 1:2^j
            fprintf(fid,'%f,', freq(i,p,q));
        end
    end
    fprintf(fid,'%s\n',class{i,1});
end
fclose(fid);



 