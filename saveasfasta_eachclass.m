%% save as FASTA file

% Import the sequence set from csv file
rawText = textread(readFile, '%s', 'whitespace',',');
% Sequence
seq = rawText(1:2:length(rawText));
% Class
class = rawText(2:2:length(rawText));

j = 1;
k = 1;
for i = 1:length(seq)
    % recognition sequences
    if class{i,1} == 'Y'
        PosSeq{j,1} = seq{i,1};
        PosSeqName{j,1} = ['TC12_rec' num2str(j)];
        j = j+1;
    % non-recognition sequences
    else
        NegSeq{k,1} = seq{i,1};
        NegSeqName{k,1} = ['TC12_nonrec' num2str(k)];
        k = k+1;
    end
end

fastaid = 'TC12_pos.txt';
fastawrite(fastaid,PosSeqName,PosSeq);

fastaid = 'TC12_neg.txt';
fastawrite(fastaid,NegSeqName,NegSeq);